module.exports = (sequelize: any, Sequelize: any) => {
    const agent = sequelize.define("Agent", {
      name: {
        type: Sequelize.STRING,
      },
      username: {
        type: Sequelize.STRING,
      },
      email: {
        type: Sequelize.STRING,
      },
      password: {
        type: Sequelize.STRING,
      },
      phone: {
        type: Sequelize.STRING,
        },
        address: {
          type: Sequelize.STRING,
        },
        
        creatorName: {
            type: Sequelize.STRING,
          }
        
    });
    return agent;
  };
  