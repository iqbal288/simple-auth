module.exports = (sequelize: any, Sequelize: any) => {
    const farming = sequelize.define("Farmer", {
      name: {
        type: Sequelize.STRING,
        allowNull:false,
      },
      spouse_or_father: {
        type: Sequelize.STRING,
        allowNull:false,
      },
      division: {
        type: Sequelize.STRING,
        allowNull:false,
      },
      district: {
        type: Sequelize.STRING,
        allowNull:false,
      },
      upazilla: {
        type: Sequelize.STRING,
        allowNull:false,
        },
        village: {
          type: Sequelize.STRING,
          allowNull:false,
        }, 
        mobile: {
          type: Sequelize.STRING,
          allowNull:false,
      },
      nid:{
        type: Sequelize.INTEGER,
        allowNull:false,
      },
      age:{
        type: Sequelize.INTEGER,
        allowNull:false,
      },
      education_level:{
        type: Sequelize.INTEGER,
        
      },
      total_members:{
        type: Sequelize.INTEGER,
      
      },
      male_members:{
        type: Sequelize.INTEGER,
    
      },
      female_members:{
        type: Sequelize.INTEGER,
       
      },
      family_type:{
         type: Sequelize.STRING,
      },
      earning_members:{
         type: Sequelize.INTEGER
      },
      school_going:{
         type: Sequelize.INTEGER
      },
      farming_experience:{
         type: Sequelize.INTEGER
      },
      cultivable_area:{
         type: Sequelize.STRING
      },
      creator_name: {
        type: Sequelize.STRING
      }
        
    });
    return farming;
  };
  