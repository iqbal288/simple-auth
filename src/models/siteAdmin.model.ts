module.exports = (sequelize: any, Sequelize: any) => {
  const SiteAdmin = sequelize.define("SiteAdmin", {
    name: {
      type: Sequelize.STRING,
    },
    username: {
      type: Sequelize.STRING,
    },
    email: {
      type: Sequelize.STRING,
    },
    password: {
      type: Sequelize.STRING,
    },
    phone: {
      type: Sequelize.STRING,
    },
  });
  return SiteAdmin;
};
