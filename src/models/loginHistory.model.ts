module.exports = (sequelize: any, Sequelize: any) => {
    const loginHistory = sequelize.define("LoginHistory", {
      name: {
        type: Sequelize.STRING,
      },
      username: {
        type: Sequelize.STRING,
      },
      email: {
        type: Sequelize.STRING,
      },
      role: {
        type: Sequelize.STRING,
        },
        logTime: {
          type: Sequelize.STRING,
        },
        
    });
    return loginHistory;
  };
  