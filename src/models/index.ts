const dbConfig = require("../database/db.config.ts");

const Sequelize = require("sequelize");
const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
  host: dbConfig.HOST,
  port: dbConfig.PORT,
  dialect: dbConfig.dialect,
  pool: {
    max: dbConfig.pool.max,
    min: dbConfig.pool.min,
    acquire: dbConfig.pool.acquire,
    idle: dbConfig.pool.idle,
  },
  dialectOptions: {
    instanceName: "instance",
    options: {
      encrypt: true,
      trustServerCertificate: true,
      requestTimeout: 30000,
    },
  },
});

const database: any = {};



database.siteAdmin = require("./siteAdmin.model.ts")(sequelize, Sequelize);
database.agent = require("./agent.model.ts")(sequelize, Sequelize);
database.farmer = require("./farmer.model.ts")(sequelize, Sequelize);
database.loginHistory = require("./loginHistory.model.ts")(sequelize, Sequelize);

Object.keys(database).forEach(modelName => {
  if (database[modelName].associate) {
    database[modelName].associate(database);
  }
});
database.Sequelize = Sequelize;
database.sequelize = sequelize;
export const DATABASE = database;
