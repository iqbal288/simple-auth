
import { GraphQLObjectType } from "graphql";
import { GET_ALL_AGENT, GET_INFORMATION } from "./Agent";
import { GET_ALL_FARMER, GET_FARMER_INFO } from "./Farmer";
import { GET_LOGIN_USER } from "./LoginHistory";
import { GET_ALL_SITE_ADMIN, SEARCH_SITE_ADMIN } from "./SiteAdmin";
const RootQuery = new GraphQLObjectType({
  name: "RootQuery",
  fields: {
    allSiteAdmin: GET_ALL_SITE_ADMIN,
      searchSiteAdmin: SEARCH_SITE_ADMIN,
      allAgent: GET_ALL_AGENT,
    getInfo: GET_INFORMATION,
    getAllFarmer: GET_ALL_FARMER,
    getFarmerInfo: GET_FARMER_INFO,
    getLoginUser:GET_LOGIN_USER
  },
});

export default RootQuery;