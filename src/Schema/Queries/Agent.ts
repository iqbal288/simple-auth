import { GraphQLList, GraphQLString } from "graphql";
import { DATABASE } from "../../models";
import { AgentType } from "../TypeDefs/Agent";
const Agent = DATABASE.agent;

export const GET_ALL_AGENT = {
    type: new GraphQLList(AgentType),
   async resolve(parent:any,args:any,context:any) {
        try {
            if (!context.req.username) {
                return[{result_code:'002',status:"Unauthenticate"}]
            }
            const allAgent = await Agent.findAll({ where: { creatorName: context.req.username } });
            if (allAgent.length === 0) {
                return([{result_code:"002", status:"agent not found"}])
            }
            else {
                return allAgent
            }
        } catch (error) {
            return {result_code:"002", status:"FAIL"}
        }
    }

}

export const GET_INFORMATION = {
    type: AgentType,
    args: {
        id:{ type: GraphQLString}
    },
   async resolve(parent: any, args: any, context: any) {
       const { id } = args;
        return await Agent.findOne({where:{id}})
    }
}