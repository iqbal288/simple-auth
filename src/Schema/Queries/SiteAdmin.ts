import { GraphQLList, GraphQLString } from "graphql";
import { DATABASE } from "../../models";
import { SiteAdminType } from "../TypeDefs/SiteAdmin";

const Users = DATABASE.users;
const SiteAdmin = DATABASE.siteAdmin;
export const GET_ALL_USER = {
  type: new GraphQLList(SiteAdminType),
  resolve() {
    return Users.findAll();
  },
};
export const GET_ALL_SITE_ADMIN = {
  type: new GraphQLList(SiteAdminType),
  resolve(parent: any, args: any, req: any) {
   
    return SiteAdmin.findAll();
  },
};

export const SEARCH_SITE_ADMIN = {
  type: new GraphQLList(SiteAdminType),
  args: {
    email:{type:GraphQLString}
  },
async resolve(parent: any, args: any, context:any) {
    const { email } = args;
 try {
  const data = await SiteAdmin.findAll({ where: { email: email } });
  if (data.length===0) {
  return ({message: 'Site Admin not found'})
  }
    return data
 } catch (error) {
   return ({status:"FAIL"})
 }
  }
}
