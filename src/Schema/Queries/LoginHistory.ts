import { GraphQLList } from "graphql";
import { DATABASE } from "../../models";
import { AgentType } from "../TypeDefs/Agent";
const LoginHistory = DATABASE.loginHistory;

export const GET_LOGIN_USER = {
    type: new GraphQLList(AgentType),
   async resolve(parent:any,args:any,context:any) {
        try {
            const allAgent = await LoginHistory.findAll();
                return allAgent
        } catch (error) {
            return {result_code:"002", status:"FAIL"}
        }
    }

}