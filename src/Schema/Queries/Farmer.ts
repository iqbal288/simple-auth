import { GraphQLInt, GraphQLList } from "graphql";
import { DATABASE } from "../../models";
import { FarmerType } from "../TypeDefs/Farmer";
const Farmer = DATABASE.farmer;
const Agent = DATABASE.agent
//get all farmer
export const GET_ALL_FARMER = {
    type: new GraphQLList(FarmerType),
   async resolve(parent: any, args: any, context: any) {
      try {
        if (!context.req.username) {
            return ([{ result_code: '002', status: "Unauthenticated" }]);
          };
          const jwtAuth = await Agent.findOne({ where: { username: context.req.username } }) 

          if (!jwtAuth) return ({ result_code: '002', status: "Unauthenticated" });
        return await Farmer.findAll({where:{creator_name:context.req.username}})
      } catch (error) {
        return ({result_code:"002",status:"FAIL "})
      }
    }

}

//get farmer info
export const GET_FARMER_INFO = {
    type: FarmerType,
    args: {
        id: {
            type: GraphQLInt
        }
    },
    async resolve(parent: any, args: any, context: any) {
        const { id } = args;
        try {
            const farmerInfo = await Farmer.findOne({ where: { id: id } });
            if (!farmerInfo) {
                return ({result_code: '002', status: "Farmer is not Found"})
            }
            return farmerInfo
        } catch (error) {
            return ({result_code:"002",status:"FAIL "})
        }
    }
}
