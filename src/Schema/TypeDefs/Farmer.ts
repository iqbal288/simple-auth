import { GraphQLID, GraphQLInt, GraphQLObjectType, GraphQLString } from "graphql";

export const FarmerType = new GraphQLObjectType({
  name: "Farmer",
  fields: () => ({
      id: { type: GraphQLID },
      ids:{ type: GraphQLInt},
    name: { type: GraphQLString },
    spouse_or_father: { type: GraphQLString },
    division: { type: GraphQLString },
    district: { type: GraphQLString },
    upazilla: { type: GraphQLString },
    village:{ type: GraphQLString},
    mobile: { type: GraphQLString },
    nid: { type: GraphQLInt },
    age: { type: GraphQLInt },
    education_level: { type: GraphQLInt },
    total_members: { type: GraphQLInt },
    male_members: { type: GraphQLInt },
    female_members: { type: GraphQLInt },
    family_type: { type: GraphQLString },
    earning_members: { type: GraphQLInt },
    school_going: { type: GraphQLInt },
    farming_experience: { type: GraphQLInt },
    cultivable_area: { type: GraphQLString },
    status: { type: GraphQLString },
    result_code: { type: GraphQLString },
    token: { type: GraphQLString },
    logTime: { type: GraphQLString },
  }),
});
