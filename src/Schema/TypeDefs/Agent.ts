import { GraphQLID, GraphQLObjectType, GraphQLString } from "graphql";

export const AgentType = new GraphQLObjectType({
  name: "Agent",
  fields: () => ({
    id: { type: GraphQLID },
    name: { type: GraphQLString },
    username: { type: GraphQLString },
    password: { type: GraphQLString },
      phone: { type: GraphQLString },
      email: { type: GraphQLString },
      address:{ type: GraphQLString},
    status: { type: GraphQLString },
      role:{ type: GraphQLString },
    result_code: { type: GraphQLString },
    token: { type: GraphQLString },
    logTime: { type: GraphQLString },
  }),
});
