import { GraphQLID, GraphQLObjectType, GraphQLString } from "graphql";

export const SiteAdminType = new GraphQLObjectType({
  name: "Site_Admin",
  fields: () => ({
    id: { type: GraphQLID },
    name: { type: GraphQLString },
    username: { type: GraphQLString },
    password: { type: GraphQLString },
    phone: { type: GraphQLString },
    status: { type: GraphQLString },
    result_code: { type: GraphQLString },
    token: { type: GraphQLString },
    email:{ type: GraphQLString},
    logTime: { type: GraphQLString },
  }),
});
