const bcrypt = require("bcrypt");
import { GraphQLID, GraphQLInt, GraphQLList, GraphQLString } from "graphql";
import { DATABASE } from "../../models";
import { FarmerType } from "../TypeDefs/Farmer";
const jwt = require("jsonwebtoken");
const Agent = DATABASE.agent;
const SiteAdmin = DATABASE.siteAdmin;
const Farmer = DATABASE.farmer;
const Sequelize = require("sequelize");
const Op = Sequelize.Op;


//create a new farmer
export const CREATE_FARMER = {
    type: FarmerType,
    args: {
        id: { type: GraphQLID },
        name: { type: GraphQLString },
        spouse_or_father: { type: GraphQLString },
        division: { type: GraphQLString },
        district: { type: GraphQLString },
        upazilla: { type: GraphQLString },
        village:{ type: GraphQLString},
        mobile: { type: GraphQLString },
        nid: { type: GraphQLInt },
        age: { type: GraphQLInt },
        education_level: { type: GraphQLInt },
        total_members: { type: GraphQLInt },
        male_members: { type: GraphQLInt },
        female_members: { type: GraphQLInt },
        family_type: { type: GraphQLString },
        earning_members: { type: GraphQLInt },
        school_going: { type: GraphQLInt },
        farming_experience: { type: GraphQLInt },
        cultivable_area: { type: GraphQLString },
        status: { type: GraphQLString },
        result_code: { type: GraphQLString },
        token: { type: GraphQLString },
        logTime: { type: GraphQLString },
    },
    async resolve(parent: any, args: any, context: any) {
        const { name, spouse_or_father, division, district, upazilla, village, mobile, nid, age, education_level, total_members, male_members, female_members, family_type, earning_members, school_going, farming_experience, cultivable_area } = args;
        try {
            if (!context.req.username) {
                return ({ result_code: '002', status: "Unauthenticated" });
            };

            const jwtAuth = await Agent.findOne({ where: { username: context.req.username } })

            if (!jwtAuth) return ({ result_code: '002', status: "Unauthenticated" });
            const existingAgent = await Farmer.findOne({ where: { nid } });
            console.log('====================================');
            console.log(context.req.username);
            console.log('====================================');
            if (existingAgent) return { result_code: '002', status: "Farmer already exists" };
           
            const createFarmer = await Farmer.create({
                name,
                spouse_or_father,
                division,
                district,
                upazilla,
                village,
                mobile,
                nid,
                age,
                education_level,
                total_members,
                male_members,
                female_members,
                family_type,
                earning_members,
                school_going,
                farming_experience,
                cultivable_area,
                creator_name:context.req.username,
            })

            return ({
                result_code: "000",
                status: "SUCCESS",
                name:createFarmer.name,
                spouse_or_father:createFarmer.spouse_or_father,
                division: createFarmer.division,
                district: createFarmer.district,
                upazilla: createFarmer.upazilla,
                village: createFarmer.village,
                mobile: createFarmer.mobile,
                nid: createFarmer.nid,
                age: createFarmer.age,
                education_level: createFarmer.education_level,
                total_members: createFarmer.total_members,
                male_members: createFarmer.male_members,
                female_members: createFarmer.female_members,
                family_type: createFarmer.family_type,
                earning_members: createFarmer.earning_members,
                school_going: createFarmer.school_going,
                farming_experience: createFarmer.farming_experience,
                cultivable_area : createFarmer.cultivable_area,
            })
        } catch (error) {
            return ({result_code:"002",status:"FAIL "})
        }
    }
    
}


//search farmer by names

export const SEARCH_FARMER = {
    type: new GraphQLList(FarmerType) ,
    args: {
        name:{ type: GraphQLString}
    },
    async resolve(parent: any, args: any, context: any) {
        const { name } = args;
        try {
            const searchResult = await Farmer.findAll({ where: { name: { [Op.like]: `%${name}%` } } })
            return searchResult
        } catch (error) {
            return ({result_code:"002",status:"FAIL "})  
        }
    }
}

//update information
export const UPDATE_FARMER_INFO = {
    type: FarmerType,
    args: {
        id: { type: GraphQLID },
        name: { type: GraphQLString },
        spouse_or_father: { type: GraphQLString },
        division: { type: GraphQLString },
        district: { type: GraphQLString },
        upazilla: { type: GraphQLString },
        village:{ type: GraphQLString},
        mobile: { type: GraphQLString },
        age: { type: GraphQLInt },
        education_level: { type: GraphQLInt },
        total_members: { type: GraphQLInt },
        male_members: { type: GraphQLInt },
        female_members: { type: GraphQLInt },
        family_type: { type: GraphQLString },
        earning_members: { type: GraphQLInt },
        school_going: { type: GraphQLInt },
        farming_experience: { type: GraphQLInt },
        cultivable_area: { type: GraphQLString },
    },
    async resolve(parent: any, args: any, context: any) {
        const { name, spouse_or_father, division, district, upazilla, village, mobile, nid, age, education_level, total_members, male_members, female_members, family_type, earning_members, school_going, farming_experience, cultivable_area,id } = args;

        try {
            const findFarmer = await Farmer.findOne({ where: { id } });
            if (!findFarmer) return { result_code: "002", status: "Farmer is not found" };
             await Farmer.update({
                name: name ? name : findFarmer.name,
                spouse_or_father: spouse_or_father ? spouse_or_father : findFarmer.spouse_or_father,
                division: division ? division : findFarmer.division,
                district: district ? district : findFarmer.district,
                upazilla: upazilla ? upazilla : findFarmer.upazilla,
                village: village ? village : findFarmer.village,
                mobile: mobile ? mobile : findFarmer.mobile,
                nid: nid ? nid : findFarmer.nid,
                age: age ? age : findFarmer.age,
                education_level: education_level ? education_level : findFarmer.education_level,
                total_members: total_members ? total_members : findFarmer.total_members,
                male_members: male_members ? male_members : findFarmer.male_members,
                female_members: female_members ? female_members : findFarmer.female_members,
                family_type: family_type ? family_type : findFarmer.family_type,
                earning_members: earning_members ? earning_members : findFarmer.earning_members,
                school_going: school_going ? school_going : findFarmer.school_going,
                farming_experience: farming_experience ? farming_experience : findFarmer.farming_experience,
                cultivable_area: cultivable_area ? cultivable_area : findFarmer.cultivable_area,

            }, { where: { id } })
            const updateFarmer = await Farmer.findOne({ where: { id } });
            return updateFarmer
        } catch (error) {
            return ({result_code:"002",status:"FAIL "})
        }
    }
}


// delete farmerInfo

export const DELETE_FARMER= {
    type: FarmerType,
    args: {
        id:{ type: GraphQLID}
    },
    async resolve(parent: any, args: any, context: any) {
        const { id } = args;
        try {
            const findFarmer = await Farmer.findOne({ where: { id } });
            if (!findFarmer) return { result_code: "002", status: "Farmer is not found" };
            await Farmer.destroy({ where: { id } });
                return ({result_code:"002",status:"Farmer deleted successfully"})
        } catch (error) {
            return ({result_code:"002",status:"FAIL "})
        }
    }
}
