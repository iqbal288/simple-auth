const bcrypt = require("bcrypt");
import { GraphQLString } from "graphql";
import { DATABASE } from "../../models";
import { SiteAdminType } from "../TypeDefs/SiteAdmin";
const jwt = require("jsonwebtoken");
const SiteAdmin = DATABASE.siteAdmin;
const LoginHistory = DATABASE.loginHistory;


///create new site admin
export const CREATE_SITE_ADMIN = {
  type: SiteAdminType,
  args: {
    name: { type: GraphQLString },
    username: { type: GraphQLString },
    password: { type: GraphQLString },
    email: { type: GraphQLString },
    phone: { type: GraphQLString },
  },
  async resolve(parent: any, args: any, context: any) {
    const { name, username, password, email, phone } = args;
    const loginTimestamp = new Date().toLocaleString()
    try {
      //check if the user already exists
      const existingUser = await SiteAdmin.findOne({ where: { email } });
      if (existingUser) return new Error ("User is already exist");

      //password hashing
      const hashPassword = await bcrypt.hash(password, 12);

      // save into the database
      const data = await SiteAdmin.create({
        name,
        username,
        email,
        phone,
        password: hashPassword,
      });

      //save into log in table
      await LoginHistory.create({
        name,
        username,
        role: 'admin',
        logTime:loginTimestamp
      })
      // generate new token
      const token = jwt.sign(
        {username: data.username },
        "test",
        {
          expiresIn: "1h",
        }
      );
      await context.res.header("x-auth-token", token);


      //return success message return
    return ({
        result_code: "000",
        name: data.name,
        username: data.username,
        id: data.id,
        phone: data.phone,
        status: "SUCCESS",
      });
    } catch (error: any) {
      // error return
      return {result_code:'002',status:"FAIL"}
    }
  },
};

// modify site admin
export const MODIFY_SITE_ADMIN = {
  type: SiteAdminType,
  args: {
    email: { type: GraphQLString },
    password: { type: GraphQLString },
    name: { type: GraphQLString },
    phone: { type: GraphQLString },
     username:{ type: GraphQLString}
  },
  async resolve(parent: any, args: any, context: any) {
    const { name, email, password, phone, username } = args;
    try {
      // find the selected side admin
      const findAdmin = await SiteAdmin.findAll({ where: { email: email } });
      if (findAdmin.length === 0) {
        return new Error( "user is not found" )
      }

      // update and save into the database with new information
      const updateUser = await SiteAdmin.update({ name, password, username,phone }, { where: { email } })
      return updateUser

    } catch (error: any) {
      //error message
      return  {result_code:"002",status:"FAIL"}
    }

  }
}


// remove site admin
export const REMOVE_SITE_ADMIN = {
  type: SiteAdminType,
  args: {
    email:{ type: GraphQLString}
  },
  async resolve(parent:any, args: any, context: any) {
    const { email } = args;
    try {
      const findSiteAdmin = await SiteAdmin.findAll({ where: { email } });
      if (findSiteAdmin.length===0) return new Error("Site Admin not found" );
      await SiteAdmin.destroy({ where: { email } });
      return {result_code:"002",status:"SITE ADMIN DELETED" };
    } catch (error) {
      return  {result_code:"002",status:"FAIL"}
    }
  }
}
// login site Admin

export const LOGIN_SITE_ADMIN = {
  type: SiteAdminType,
  args: {
    username: { type: GraphQLString },
    password: { type: GraphQLString },
  },
  async resolve(parent: any, args: any, context: any) {
    const { username, password } = args;
    const loginTimestamp = new Date().toLocaleString()
    try {
      const findUser = await SiteAdmin.findOne({ where: { username } });
      if (!findUser) return { result_code: "002", status: "admin is not found for this username" };
      const verifyPasswords = await bcrypt.compare( password,findUser.password)
      if (!verifyPasswords) return { result_code: "002", status: "INVALID credentials" }
      await LoginHistory.create({
        name:findUser.name,
        username,
        role: 'admin',
        logTime:loginTimestamp
      })
      const token = jwt.sign(
        {username: findUser.username },
        "test",
        {
          expiresIn: "1h",
        }
      );
      await context.res.header("x-auth-token", token);
      return({
        result_code: "000",
        name: findUser.name,
        username: findUser.username,
        id: findUser.id,
        phone: findUser.phone,
        status: "SUCCESS",
      });
      
    } catch (error) {
      
    }
  }
}