const bcrypt = require("bcrypt");
import { GraphQLID, GraphQLString } from "graphql";
import { DATABASE } from "../../models";
import { AgentType } from "../TypeDefs/Agent";
const jwt = require("jsonwebtoken");
const Agent = DATABASE.agent;
const SiteAdmin = DATABASE.siteAdmin;
const LoginHistory = DATABASE.loginHistory;


//create new agent
export const CREATE_AGENT = {
    type: AgentType,
    args: {
        name: { type: GraphQLString },
        username: { type: GraphQLString },
        password: { type: GraphQLString },
        phone: { type: GraphQLString },
        email: { type: GraphQLString },
        address:{ type: GraphQLString},
    },
    async resolve(parent: any, args: any, context: any) {

        const { name, username, password, phone, email, address } = args;
        const loginTimestamp = new Date().toLocaleString()
      try {
        if (!context.req.username) {
            return ({result_code:'002',status:"Unauthenticated"})
          }
          const jwtAuth = await SiteAdmin.findOne({ where: { username: context.req.username } })
          if (!jwtAuth) return ({ result_code: '002', status: "Unauthenticated" });
        const existingAgent = await Agent.findOne({ where: { username } });
        if(existingAgent) return {result_code:'002',status:"agent already exists"}
        const hashPassword = await bcrypt.hash(password, 12);
        const createAgent = await Agent.create({
            password: hashPassword,
            phone, email, address, name, username, creatorName: context.req.username
        });
        await LoginHistory.create({
            name,
            username,
            role: 'agent',
            logTime:loginTimestamp
          })
        const token = jwt.sign(
            {username: createAgent.username },
            "test",
            {
              expiresIn: "1h",
            }
        );
        await context.res.header("x-auth-token", token);
        return({
            result_code: "000",
            name: createAgent.name,
            username: createAgent.username,
            id: createAgent.id,
            phone: createAgent.phone,
            status: "SUCCESS",
          });
      } catch (error) {
          return ({result_code:"002",status:"FAIL "})
      }
    }
}

//update agent

export const UPDATE_AGENT = {
    type: AgentType,
    args: {
        name: { type: GraphQLString },
        email: { type: GraphQLString },
        phone: { type: GraphQLString },
        address: { type: GraphQLString },
        id:{ type: GraphQLID}
    },
    async resolve(parent: any, args: any, context: any) {
        const { name, email, phone, address, id } = args;
       
        try {
            await Agent.update({ name, email, phone, address }, { where: { id } });
       
      const updatedUser = await Agent.findOne({where:{id}});
  
        return updatedUser
        } catch (error) {
            return ({result_code:"002",status:"FAIL "})
        }
    }
}

//delete agent
export const DELETE_AGENT = {
    type: AgentType,
    args: {
        id:{type: GraphQLID}
    },
    async resolve(parent: any, args: any, context: any) {
        const { id } = args;
        const admin = context.req.username
        try {
            const findAdmin = await SiteAdmin.findOne({ where: { username: admin } })
            if(!admin) return {result_code:"002", status:"FAIL"}
            await Agent.destroy({ where: { id } })
            return{result_code:"000",status:"Agent is deleted successfully"}
        } catch (error) {
            return ({result_code:"002",status:"FAIL "}) 
        }
    }
}
export const AGENT_LOGIN = {
    type: AgentType,
    args: {
        username: { type: GraphQLString },
        password: { type: GraphQLString },
    },
    async resolve(parent: any, args: any, context: any) {
        const { username, password } = args;
        const loginTimestamp = new Date().toLocaleString()
        try {
            const findAgent = await Agent.findOne({ where: { username } });
            
            if (!findAgent) return { result_code: "002", status: "User is not found" }
            const verifyAccount = await bcrypt.compare(password, findAgent.password);

            if (!verifyAccount) return { result_code: "002", status: "Invalid Credentials" }
            await LoginHistory.create({
                name:findAgent.name,
                username,
                role: 'agent',
                logTime:loginTimestamp
              })
            const token = jwt.sign(
                {username: findAgent.username },
                "test",
                {
                  expiresIn: "1h",
                }
            );
            context.res.header("x-auth-token", token)
            return {
                result_code: "000",
                status: "valid",
                id: findAgent.id,
                username: findAgent.username,
                name: findAgent.name,
                email: findAgent.email,
                phone: findAgent.phone,

            }
        } catch (error) {
            return ({result_code:"002",status:"FAIL "})
        }
    }
}


