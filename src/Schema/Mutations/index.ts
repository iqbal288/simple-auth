
import { GraphQLObjectType } from "graphql";
import { AGENT_LOGIN, CREATE_AGENT, DELETE_AGENT, UPDATE_AGENT } from "./Agent";
import { CREATE_FARMER, DELETE_FARMER, SEARCH_FARMER, UPDATE_FARMER_INFO } from "./Farmer";
import { CREATE_SITE_ADMIN, LOGIN_SITE_ADMIN, MODIFY_SITE_ADMIN, REMOVE_SITE_ADMIN } from "./SiteAdmin";
const Mutation = new GraphQLObjectType({
    name: "Mutation",
    fields: {
      createSiteAdmin: CREATE_SITE_ADMIN,
      updateSiteAdmin: MODIFY_SITE_ADMIN,
      deleteSiteAdmin: REMOVE_SITE_ADMIN,
    loginSiteAdmin: LOGIN_SITE_ADMIN,
    agentCreate: CREATE_AGENT,
        updateAgent: UPDATE_AGENT,
        deleteAgent: DELETE_AGENT,
      loginAgent: AGENT_LOGIN,
      createFarmer: CREATE_FARMER,
      searchFarmer: SEARCH_FARMER,
      updateFarmer: UPDATE_FARMER_INFO,
      farmerDelete:DELETE_FARMER
    },
});
  export default Mutation;