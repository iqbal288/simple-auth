import { GraphQLSchema } from "graphql";
import Mutation from "./Mutations";
import RootQuery from "./Queries";



export const schema = new GraphQLSchema({
  query: RootQuery,
  mutation: Mutation,
});
