const cors = require("cors");
const express = require("express");
const { graphqlHTTP } = require("express-graphql");
const { DATABASE } = require("./models");
const { schema } = require("./Schema/index");
const isAuth = require("./middleware/isAuth");
const imageRoute = require("./routes/imageRoute.ts")


const main = async () => {
  const app = express();
  app.use(cors());
  app.use(express.json());

  DATABASE.sequelize.sync();
  app.use(isAuth);

  app.use("/api/users", imageRoute);



  app.use("/graphql", (req: any, res: any) => {
    return graphqlHTTP({
      schema: schema,
      graphiql: true,
      playground:true,
      context: { req, res },
    })(req, res);
  });

  app.listen(3001, () => {
    console.log("server running on port 3001");
  });
};
main().catch((err) => {
  console.log(err);
});
