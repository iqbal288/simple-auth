const router = require("express").Router();
const userController = require("../controller/userControl.ts");

router.post("/", userController.setProfilePic);

module.exports = router;